void render_background(){
	u32* pixel = (u32*)render_state.memory;
	for (int h = 0; h < render_state.height; h++) {
		for (int w = 0; w < render_state.width; w++) {
			*pixel++ = h * h * w * w;
		}
	}
}
void clear_screen(u32 color) {

	u32* pixel = (u32*)render_state.memory;
	for (int h = 0; h < render_state.height; h++) {
		for (int w = 0; w < render_state.width; w++) {
			*pixel++ = color;
		}
	}
}

float render_scale = 0.01f;


void draw_rect_in_pixels(int x0, int y0, int x1, int y1, u32 color) {
	x0 = clamp(0, x0, render_state.width);
	x1 = clamp(0, x1, render_state.width);
	y0 = clamp(0, y0, render_state.height);
	y1 = clamp(0, y1, render_state.height);
	
	for (int h = y0; h < y1; h++) {
		u32* pixel = (u32*)render_state.memory + x0 + h * render_state.width;
		for (int w = x0; w < x1; w++) {
			*pixel++ = color;
		}
	}
}


void draw_rect(float x, float y, float half_size_x, float half_size_y, u32 color) {

	x *= render_state.height * render_scale;
	y *= render_state.height * render_scale;
	half_size_x *= render_state.height * render_scale;
	half_size_y *= render_state.height * render_scale;

	x += render_state.width / 2.f;
	y += render_state.height / 2.f;
	
	// Change to pixels
	int x0 = x - half_size_x;
	int x1 = x + half_size_x;
	int y0 = y - half_size_y;
	int y1 = y + half_size_y;

	draw_rect_in_pixels(x0, y0, x1, y1, color);
}
